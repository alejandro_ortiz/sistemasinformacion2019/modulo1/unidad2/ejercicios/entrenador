﻿USE provincias;

-- ¿Qué autonomías tienen nombre compuesto? Ordena el resultado alfabéticamente en orden inverso
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.autonomia LIKE '% %' 
  ORDER BY p.autonomia DESC;

-- ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.autonomia NOT LIKE '% %' 
  ORDER BY p.autonomia DESC;

-- ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.provincia LIKE '% %' 
  ORDER BY p.autonomia;

-- Autonomías que comiencen por 'can' ordenadas alfabéticamente
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.autonomia LIKE 'can%' 
  ORDER BY p.autonomia;

-- Listado de provincias y autonomías que contengan la letra ñ
SELECT provincia
  FROM provincias  
  WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
UNION
SELECT autonomia 
  FROM provincias 
  WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;

-- Superficie del país
SELECT SUM(p.superficie) 
  FROM provincias p;

-- En un listado alfabético, ¿qué provincia estaría la primera?
SELECT MIN(p.provincia) 
  FROM provincias p;

-- ¿Qué provincias tienen un nombre más largo que el de su autonomía?
SELECT p.provincia 
  FROM provincias p 
  WHERE CHAR_LENGTH(p.provincia)>CHAR_LENGTH(p.autonomia);

-- ¿Cuántas comunidades autónomas hay?
SELECT COUNT(DISTINCT p.autonomia) 
  FROM provincias p;

-- ¿Cuánto mide el nombre de provincia más largo?
SELECT MAX(CHAR_LENGTH(p.provincia)) 
  FROM provincias p;