﻿USE provincias;

-- Obtén el listado de autonomías (una línea por autonomía), junto al listado de sus 
-- provincias en una única celda. Recuerda que, tras una coma, debería haber un espacio en blanco.

  SELECT p.autonomia, GROUP_CONCAT(p.provincia SEPARATOR ', ') 
    FROM provincias p 
    GROUP BY p.autonomia;

-- Listado del número de provincias por autonomía ordenadas de más a menos provincias 
-- y por autonomía en caso de coincidir

  SELECT COUNT(*)provincias, p.autonomia 
    FROM provincias p 
    GROUP BY p.autonomia 
    ORDER BY provincias DESC, p.autonomia ASC;

-- ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?
  SELECT p.autonomia, COUNT(*) 
    FROM provincias p 
    WHERE p.provincia LIKE '% %' 
    GROUP BY p.autonomia;

-- Autonomías uniprovinciales
  SELECT p.autonomia 
    FROM provincias p 
    GROUP BY p.autonomia 
    HAVING COUNT(*)=1;

-- ¿Qué autonomía tiene 5 provincias?
  SELECT p.autonomia 
    FROM provincias p 
    GROUP BY p.autonomia 
    HAVING COUNT(*)=5;

-- Población de la autonomía más poblada
  SELECT MAX(pob) 
    FROM (
      SELECT SUM(p.poblacion) pob 
        FROM provincias p 
        GROUP BY p.autonomia)c1;

-- ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?
  SELECT (
    SELECT p1.poblacion 
      FROM provincias p1 
      WHERE p1.provincia='Cantabria')/(SUM(p.poblacion))*100 , (
    SELECT p1.superficie 
      FROM provincias p1 
      WHERE p1.provincia='Cantabria')/(SUM(p.superficie))*100 
    FROM provincias p;

-- Obtener la provincia más poblada de cada comunidad autónoma, indicando la población de ésta.
-- Mostrar autonomía, provincia y población por orden de aparición en la tabla.
  SELECT autonomia, provincia, poblacion 
    FROM (
      SELECT autonomia, MAX(poblacion) poblacion
      FROM provincias 
      GROUP BY 1
    ) c1 
    JOIN provincias 
  USING (autonomia, poblacion);

-- ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria?
  SELECT COUNT(*) 
    FROM(
      SELECT autonomia, SUM(poblacion)p 
        FROM provincias  
        GROUP BY autonomia 
        HAVING p>=(
          SELECT SUM(poblacion) 
            FROM provincias 
            WHERE autonomia='Cantabria')
        ) c1 ;

-- Automía más extensa

  SELECT autonomia 
    FROM (
      SELECT autonomia, SUM(superficie)sup 
        FROM provincias 
        GROUP BY autonomia
      )c1 
    WHERE sup=(
      SELECT MAX(sup) 
        FROM (
          SELECT autonomia, SUM(superficie)sup 
            FROM provincias  
            GROUP BY autonomia
          )c1 
      );