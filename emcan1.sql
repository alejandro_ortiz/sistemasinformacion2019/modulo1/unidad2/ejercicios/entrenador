﻿USE emcan;

-- ¿Cuántos centros hay censados en Cantabria?

  SELECT COUNT(*) 
    FROM centros;

-- ¿Cuántos cursos se han programado?
  SELECT COUNT(*) 
    FROM programacion;

-- ¿Cuántos centros han resultado adjudicatarios de estos cursos?
  SELECT COUNT(DISTINCT cod_centro) 
    FROM programacion;

-- ¿En cuántos municipios se imparten cursos?
  SELECT COUNT(DISTINCT municipio) 
    FROM centros 
    JOIN programacion 
    USING (cod_centro);

-- ¿Cuántos cursos de informática se imparten en Cantabria?
  SELECT codigo FROM cursos WHERE codigo LIKE 'IFC%';

  SELECT COUNT(*) 
    FROM (
      SELECT codigo 
      FROM cursos 
      WHERE codigo LIKE 'IFC%'
    )c1 
    JOIN programacion 
    USING (codigo);

  -- otra forma
  SELECT COUNT(codigo) 
    FROM (
      SELECT fml 
      FROM familias 
      WHERE familia LIKE '%informática%'
    )c1 
    JOIN programacion 
    ON fml=SUBSTR(codigo,1,3);

-- ¿Cuántos cursos de informática diferentes se imparten en Cantabria?
  SELECT COUNT(DISTINCT codigo) 
    FROM (
      SELECT fml 
      FROM familias 
      WHERE familia LIKE '%informática%'
    )c1 
    JOIN programacion 
    ON fml=SUBSTR(codigo,1,3);

-- ¿Cuántos cursos de informática de nivel 3 se imparten en Cantabria?

  SELECT COUNT(codigo) 
    FROM (
      SELECT fml 
      FROM familias 
      WHERE familia LIKE '%informática%'
    )c1 
    JOIN cursos 
    ON fml=SUBSTR(codigo,1,3) AND nivel=3
  JOIN programacion USING(codigo);

-- ¿Cuántos cursos de informática de nivel 3 diferentes se imparten en Cantabria?
  SELECT COUNT(DISTINCT codigo) 
    FROM (
      SELECT fml 
      FROM familias 
      WHERE familia LIKE '%informática%'
    )c1 
    JOIN cursos 
    ON fml=SUBSTR(codigo,1,3) AND nivel=3
  JOIN programacion USING(codigo);

-- Si acabas de cursar mate y lengua N2 o tienes la ESO, 
-- ¿dónde tendrías que hacer un curso en Santander para poder cursar en Alpe Aplicaciones Web? 
-- Indica curso, centro, dirección postal y fecha de inicio, ordenando el listado por ésta última. 
-- Recuerda que un certificado de nivel 2 da acceso a un nivel 3 de su misma familia profesional y
-- que, para cursar un nivel 2 necesitas la ESO y un nivel 3 Bachiller, o un ciclo formativo grado
-- medio o superior en formación profesional, respectivamente.

  -- cursos de nivel 2
  SELECT codigo 
    FROM cursos 
    WHERE nivel=2 AND codigo l;

  -- codigos de cursos nivel 2 y codigos de centros donde se imparten
  SELECT DISTINCT codigo, cod_centro 
    FROM (
      SELECT codigo, especialidad 
        FROM cursos 
        WHERE nivel=2
      )c1 
      JOIN programacion 
      USING (codigo);

  SELECT DISTINCT c2.especialidad , centro, direccion, c2.inicio
    FROM (
      SELECT c1.especialidad, cod_centro, inicio 
        FROM (
          SELECT codigo, especialidad 
            FROM cursos 
            WHERE nivel=2 AND codigo LIKE 'IFC%'
          )c1 
          JOIN programacion 
          USING (codigo)
        )c2 
        JOIN centros 
        USING (cod_centro) 
        WHERE municipio='Santander'
        ORDER BY c2.inicio;

  -- otro forma
    SELECT especialidad , centro, direccion, inicio
    FROM programacion JOIN(
      SELECT codigo, especialidad FROM cursos
         WHERE nivel=2 AND SUBSTR(codigo,1,3)=(
            SELECT DISTINCT SUBSTR(codigo,1,3)
               FROM cursos WHERE especialidad
                  LIKE'%aplicaciones%web%'
               )
            )cursos USING(codigo) JOIN(
               SELECT * from centros
                 WHERE municipio='santander'
            ) centros USING(cod_centro)
         ORDER BY inicio;

-- Si acabas de cursar mate y lengua N2 o tienes la ESO, 
-- ¿dónde tendrías que hacer un curso en Santander para poder cursar en Alpe Formación
-- para el Empleo? Indica curso, centro, dirección postal y fecha de inicio, ordenando
-- el listado por ésta última. Recuerda que un certificado de nivel 2 da acceso a un 
-- nivel 3 de su misma familia profesional y que, para cursar un nivel 2 necesitas 
-- la ESO y para un nivel 3 Bachiller, o un ciclo formativo grado medio o superior
-- en formación profesional, respectivamente.

  SELECT especialidad , centro, direccion, inicio
    FROM programacion JOIN(
      SELECT codigo, especialidad FROM cursos
         WHERE nivel=2 AND SUBSTR(codigo,1,3)=(
            SELECT DISTINCT SUBSTR(codigo,1,3)
               FROM cursos WHERE especialidad
                  LIKE'%formacion%empleo%'
               )
            )cursos USING(codigo) JOIN(
               SELECT * from centros
                 WHERE municipio='santander'
            ) centros USING(cod_centro)
         ORDER BY inicio;