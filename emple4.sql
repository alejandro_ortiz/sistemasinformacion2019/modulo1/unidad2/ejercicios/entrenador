﻿USE emple_depart;

-- Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. 
-- Ordenar el resultado por apellido y oficio de forma ascendente
  SELECT * 
    FROM emple e
    WHERE e.dept_no= 10 
    AND e.oficio='Analista' 
    ORDER BY e.apellido, e.oficio;

-- Realizar un listado de los distintos meses en que los empleados se han dado de alta
  SELECT DISTINCT MONTH(e.fecha_alt) 
    FROM emple e;

-- Realizar un listado de los distintos años en los que los empleados se han dado de alta
  SELECT DISTINCT YEAR(e.fecha_alt) 
    FROM emple e;

-- Realizar un listado de los distintos días del mes en que los empleados se han dado de alta 
  SELECT DISTINCT DAY(e.fecha_alt) 
    FROM emple e;

-- Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 
-- o que pertenezcan al departamento número 20
  SELECT apellido 
    FROM emple 
    WHERE salario>2000 
    OR dept_no=20;

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. 
-- Listar el apellido de los empleados
  SELECT e.apellido 
    FROM emple e 
    WHERE e.apellido LIKE 'A%' 
    OR e.apellido LIKE 'M%';

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. 
-- Listar el apellido de los empleados
  SELECT apellido 
    FROM emple 
    WHERE SUBSTR(apellido,1,1)='A';

-- Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y 
-- el oficio tenga una E en cualquier posición. Ordenar la salida por oficio ascendente y
-- por salario de forma descendente
  SELECT * 
    FROM emple 
    WHERE apellido LIKE 'A%' 
    AND oficio LIKE '%E%' 
    ORDER BY oficio, salario DESC;

-- Indicar el número de empleados que hay
  SELECT COUNT(*) 
    FROM emple e;

-- Indicar el número de departamentos que hay
  SELECT COUNT(*) 
    FROM depart;
 
     