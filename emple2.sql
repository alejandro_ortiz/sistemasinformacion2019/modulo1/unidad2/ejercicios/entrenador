﻿USE emple_depart;

-- Mostrar los datos de los empleados cuyo oficio sea ANALISTA
  SELECT * 
    FROM emple e 
    WHERE e.oficio='ANALISTA';

-- Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000
  SELECT * 
    FROM emple e 
    WHERE e.oficio='ANALISTA' 
    AND e.salario>2000;

-- Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre
  SELECT e.apellido 
    FROM emple e 
    ORDER BY e.oficio, e.apellido;

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. 
-- Listar todos los campos de la tabla empleados
  SELECT * 
    FROM emple e 
    WHERE e.apellido NOT LIKE '%Z';

-- Mostrar el código de los empleados cuyo salario sea mayor que 2000
  SELECT e.emp_no 
    FROM emple e 
    WHERE e.salario>2000;

-- Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
  SELECT e.emp_no, e.apellido 
    FROM emple e 
    WHERE e.salario<2000;

-- Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500
  SELECT * 
    FROM emple e 
    WHERE e.salario BETWEEN 1500 AND 2500;

-- Seleccionar el apellido y oficio de los empleados del departamento número 20
  SELECT e.apellido, e.oficio 
    FROM emple e 
    WHERE e.dept_no=20;

-- Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados
-- por apellido de forma ascendente.
  SELECT * 
    FROM emple e 
    WHERE e.apellido LIKE 'M%' 
    OR e.apellido LIKE 'N%' 
    ORDER BY e.apellido;

-- Seleccionar los datos de los empleados cuyo oficio sea VENDEDOR.
-- Mostrar los datos ordenados por apellido de forma ascendente.
  SELECT * 
    FROM emple e 
    WHERE e.oficio='Vendedor' 
    ORDER BY e.apellido;
