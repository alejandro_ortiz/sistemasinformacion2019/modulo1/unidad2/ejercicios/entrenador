﻿USE emple_depart;

-- Mostrar todos los campos y todos los registros de la tabla empleado
  SELECT * 
    FROM emple e;

-- Mostrar todos los campos y todos los registros de la tabla departamento
  SELECT * 
    FROM depart d;

-- Mostrar el número, nombre y localización de cada departamento
  SELECT d.dept_no, d.dnombre, d.loc 
    FROM depart d;

-- Datos de los empleados ordenados por número de departamento descendentemente
  SELECT * 
    FROM emple e 
    ORDER BY e.dept_no DESC;

-- Datos de los empleados ordenados por número de departamento descendentemente 
-- y por oficio ascendente
  SELECT * 
    FROM emple e 
    ORDER BY e.dept_no DESC, e.oficio ASC;

-- Datos de los empleados ordenados por número de departamento descendentemente 
-- y por apellido ascendentemente
  SELECT * 
    FROM emple e 
    ORDER BY e.dept_no DESC, e.apellido;

-- Mostrar el apellido y oficio de cada empleado
  SELECT e.apellido, e.oficio 
    FROM emple e;

-- Mostrar localización y número de cada departamento
  SELECT d.loc, d.dept_no 
    FROM depart d;

-- Datos de los empleados ordenados por apellido de forma ascendente
  SELECT * 
    FROM emple e 
    ORDER BY e.apellido;

-- Datos de los empleados ordenados por apellido de forma descendente
  SELECT * 
    FROM emple e 
    ORDER BY e.apellido DESC;