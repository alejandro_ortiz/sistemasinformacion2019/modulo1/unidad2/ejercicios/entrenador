﻿USE provincias;

-- ¿Qué provincias están en autonomías con nombre compuesto?
SELECT p.provincia 
  FROM provincias p 
  WHERE p.autonomia LIKE '% %';

-- ¿Qué provincias tienen nombre compuesto?
SELECT p.provincia 
  FROM provincias p 
  WHERE p.provincia LIKE '% %';

-- ¿Qué provincias tienen nombre simple?
SELECT p.provincia 
  FROM provincias p 
  WHERE p.provincia NOT LIKE '% %';

-- Muestra las provincias de Galicia, indicando si es Grande, Mediana o Pequeña en función
-- de si su población supera el umbral de un millón o de medio millón de habitantes.
SELECT p.provincia,
  CASE 
    WHEN p.poblacion>1e6 THEN 'Grande'
    WHEN p.poblacion>5e5 THEN 'Mediana'
    ELSE 'Pequeña'
  END
  FROM provincias p
  WHERE p.autonomia='Galicia';

-- Autonomías terminadas en 'ana'
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.autonomia LIKE '%ana';

-- ¿Cuántos caracteres tiene cada nombre de comunidad autónoma? 
-- Ordena el resultado por el nombre de la autonomía de forma descendente
SELECT DISTINCT p.autonomia, CHAR_LENGTH(p.autonomia) caracteres 
  FROM provincias p 
  ORDER BY p.autonomia DESC;

-- ¿Qué autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente
SELECT DISTINCT p.autonomia 
  FROM provincias p 
  WHERE p.poblacion>1e6 
  ORDER BY p.autonomia;

-- Población del país
SELECT SUM(p.poblacion) poblacionPais
  FROM provincias p;

-- ¿Cuántas provincias hay en la tabla?
SELECT COUNT(*) provincias FROM provincias p;

-- ¿Qué comunidades autónomas contienen el nombre de una de sus provincias?
SELECT p.autonomia 
  FROM provincias p 
  WHERE LOCATE(p.provincia,p.autonomia)>0;