﻿USE provincias;

-- Listado de provincias
SELECT p.provincia 
  FROM provincias p;

-- ¿Cuánto suman 2 y 3?
SELECT 2+3;

-- Densidades de población de las provincias
SELECT p.provincia, p.poblacion/p.superficie AS densidad 
  FROM provincias p;

-- Obtén el listado del nombre de las provincias que tiene cada autonomía
SELECT p.autonomia, p.provincia 
  FROM provincias p;

-- ¿Cuánto vale la raíz cuadrada de 2?
SELECT SQRT(2);

-- ¿Cuántos caracteres tiene cada nombre de provincia?
SELECT p.provincia, CHARACTER_LENGTH(p.provincia) caracteres 
  FROM provincias p;

-- Listado de autonomías
SELECT DISTINCT p.autonomia 
  FROM provincias p;

-- Provincias con el mismo nombre que su comunidad autónoma
SELECT p.provincia 
  FROM provincias p 
  WHERE p.autonomia=p.provincia;

-- Provincias que contienen el diptongo 'ue'
SELECT p.provincia 
  FROM provincias p 
  WHERE p.provincia LIKE '%ue%';

-- Provincias que empiezan por A
SELECT p.provincia 
  FROM provincias p 
  WHERE p.provincia LIKE 'A%';