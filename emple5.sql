﻿USE emple_depart;

-- Contar el número de empleados cuyo oficio sea VENDEDOR
  SELECT COUNT(*) 
    FROM emple e 
    WHERE e.oficio='VENDEDOR';

-- Realizar un listado donde nos coloque el apellido del empleado y el nombre del
-- departamento al que pertenece. Ordena el resultado por apellido.
  SELECT e.apellido, d.dnombre 
    FROM emple e JOIN depart d 
    USING (dept_no) 
    ORDER BY e.apellido;

-- Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado
-- y el nombre del departamento al que pertenece. Ordenar los resultados por apellido 
-- de forma descendente.

  SELECT e.apellido, e.oficio, d.dnombre 
    FROM emple e 
    JOIN depart d 
    USING (dept_no)
    ORDER BY e.apellido DESC;

-- Mostrar los apellidos del empleado que más gana
  SELECT apellido 
    FROM emple 
    WHERE salario = (
      SELECT MAX(salario) 
        FROM emple);

-- Indicar el número de empleados más el número de departamentos
  SELECT (
    SELECT COUNT(*) 
      FROM emple e)
    +
    (SELECT COUNT(*) 
      FROM depart d) AS suma ;

-- Listar el número de departamento de aquellos departamentos que tengan empleados 
-- y el número de empleados que tengan.
  SELECT  dept_no, COUNT(*)NUMERO_DE_EMPLEADOS 
    FROM emple
    GROUP BY dept_no;
