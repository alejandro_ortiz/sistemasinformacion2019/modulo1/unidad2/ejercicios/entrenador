﻿USE ciclistas;

-- Etapas con puertos de más 1300 metros de altura

  SELECT DISTINCT numetapa 
    FROM puerto 
    WHERE altura>1300
    ORDER BY numetapa;

-- Cuántos puertos ha ganado Alfonso Gutiérrez llevando algún maillot

  SELECT dorsal FROM ciclista WHERE nombre='Alfonso Gutiérrez';

  SELECT COUNT(*) 
    FROM (SELECT numetapa,dorsal FROM puerto 
      WHERE dorsal=(
      SELECT dorsal 
        FROM ciclista 
        WHERE nombre='Alfonso Gutiérrez')
    )c2 
    JOIN lleva 
    USING(numetapa,dorsal);

-- ¿Qué ciclista gana la vuelta?
  SELECT dorsal FROM lleva WHERE numetapa=(SELECT MAX(numetapa) FROM etapa) 
    AND código=(SELECT código FROM maillot WHERE color='amarillo');

  SELECT nombre FROM (SELECT dorsal FROM lleva WHERE numetapa=(SELECT MAX(numetapa) FROM etapa) 
    AND código=(SELECT código FROM maillot WHERE color='amarillo'))c1 JOIN ciclista USING(dorsal);
    
-- Genera un ranking del importe en premios de cada equipo. 
-- Muestra importe y equipo, ordenados por importe descendente y nombre del equipo.
  
  -- dorsal y premio que se lleva por maillot
  SELECT dorsal, premio 
    FROM maillot 
    JOIN lleva 
    USING (código);

  SELECT SUM(premio)importe, nomequipo 
    FROM (
      SELECT dorsal, premio 
        FROM maillot 
        JOIN lleva 
        USING (código)
      )c1 
      JOIN ciclista 
      USING (dorsal)
    GROUP BY nomequipo
    ORDER BY importe DESC, nomequipo;

-- Obtener los números de las etapas que no tienen puertos de montaña

  SELECT DISTINCT numetapa 
    FROM puerto;

  SELECT DISTINCT numetapa 
    FROM etapa 
    WHERE numetapa NOT IN (
      SELECT DISTINCT numetapa 
        FROM puerto);

  -- CON LEFT JOIN
  SELECT c1.numetapa FROM(
    SELECT numetapa FROM etapa
  )c1 LEFT JOIN(
    SELECT DISTINCT numetapa FROM puerto
  )c2 ON c1.numetapa= c2.numetapa
  WHERE c2.numetapa IS NULL;

-- Obtener las poblaciones que tienen la meta de alguna etapa, 
-- pero desde las que no se realiza ninguna salida. 
-- Resuélvela con un producto externo
  SELECT salida FROM etapa;
  
  SELECT llegada FROM etapa;

  SELECT llegada 
    FROM etapa 
    LEFT JOIN (
      SELECT salida 
        FROM etapa
      )c1 
    ON llegada=c1.salida 
    WHERE c1.salida IS NULL;

-- Genera una tabla en el que figure el nombre del ciclista y los premios acumulados,
-- ordenada por premio descencente y, en caso de coincidir, por nombre

  -- dorsal y premio que se lleva por maillot
  SELECT dorsal, premio 
    FROM maillot 
    JOIN lleva 
    USING (código);

  SELECT nombre,SUM(premio)importe 
    FROM (
      SELECT dorsal, premio 
        FROM maillot 
        JOIN lleva 
        USING (código)
      )c1 
      JOIN ciclista 
      USING (dorsal)
    GROUP BY nombre
    ORDER BY importe DESC, nombre;

-- En cuántas etapas ha llevado maillot Induráin con puerto y además las ha ganado

  SELECT COUNT(*) FROM(
    SELECT dorsal FROM ciclista WHERE nombre ='Miguel Induráin'
  )c1 JOIN(
    SELECT DISTINCT numetapa,dorsal FROM etapa JOIN(
      SELECT numetapa FROM puerto
    )p USING(numetapa)
  )c2 USING(dorsal) JOIN lleva
  ON c2.dorsal=lleva.dorsal AND c2.numetapa=lleva.numetapa;

-- Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

  SELECT COUNT(*) FROM(
    SELECT c2.numetapa FROM (
      SELECT DISTINCT dorsal,numetapa FROM(
        SELECT dorsal FROM ciclista WHERE nombre ='Miguel Induráin'
      )c1 JOIN lleva USING(dorsal)
    )c2 JOIN etapa ON c2.dorsal=etapa.dorsal AND c2.numetapa=etapa.numetapa
  )c3 JOIN puerto USING(numetapa);

-- Obtener el nombre y la edad de los ciclistas que han llevado dos 
-- o más maillots en una misma etapa

  SELECT dorsal, numetapa, COUNT(*) 
    FROM lleva 
    GROUP BY dorsal,numetapa 
    HAVING COUNT(*)>=2;

  SELECT nombre, edad 
    FROM ciclista 
    JOIN (
      SELECT dorsal, numetapa, COUNT(*) 
        FROM lleva 
        GROUP BY dorsal,numetapa 
        HAVING COUNT(*)>=2
      )c1 USING (dorsal); 

-- Código de los maillots que han sido llevados por más de un equipo
  SELECT código FROM (
    SELECT DISTINCT código,nomequipo
      FROM lleva JOIN ciclista USING(dorsal)
    )c2 
    GROUP BY código
    HAVING COUNT(*)>1;