﻿USE provincias;

-- ¿Cuánto mide el nombre de autonomía más corto?
  SELECT MIN(CHAR_LENGTH(p.autonomia)) 
    FROM provincias p;

-- Población media de las provincias entre 2 y 3 millones de habitantes sin decimales
  SELECT ROUND(AVG(p.poblacion)) 
    FROM provincias p 
    WHERE p.poblacion BETWEEN 2e6 AND 3e6;

-- Listado de autonomías cuyas provincias lleven alguna tilde en su nombre
  SELECT DISTINCT p.autonomia 
    FROM provincias p 
    WHERE p.provincia LIKE '%á%' COLLATE utf8_bin 
    OR p.provincia LIKE '%é%' COLLATE utf8_bin 
    OR p.provincia LIKE '%í%' COLLATE utf8_bin 
    OR p.provincia LIKE '%ó%' COLLATE utf8_bin 
    OR p.provincia LIKE '%ú%' COLLATE utf8_bin;

-- Provincia más poblada
  SELECT p.provincia 
    FROM provincias p 
    WHERE p.poblacion=(
      SELECT MAX(p.poblacion) 
        FROM provincias p);

-- Provincia más poblada de las inferiores a 1 millón de habitantes
  SELECT p.provincia 
    FROM provincias p 
    WHERE p.poblacion=(
      SELECT MAX(p.poblacion) 
        FROM provincias p 
        WHERE p.poblacion<1e6);

-- Provincia menos poblada de las superiores al millón de habitantes
  SELECT p.provincia 
    FROM provincias p 
    WHERE p.poblacion=(
      SELECT MIN(p.poblacion) 
        FROM provincias p 
        WHERE p.poblacion>1e6);

-- ¿En qué autonomía está la provincia más extensa?
  SELECT p.autonomia 
    FROM provincias p 
    WHERE p.superficie=(
      SELECT MAX(p.superficie) 
        FROM provincias p);

-- ¿Qué provincias tienen una población por encima de la media nacional?
  SELECT p.provincia 
    FROM provincias p 
    WHERE p.poblacion> (
      SELECT AVG(p1.poblacion) 
        FROM provincias p1); 

-- Densidad de población del país
  SELECT (
    SELECT SUM(p.poblacion) 
      FROM provincias p
    )/(
    SELECT SUM(p.superficie) 
      FROM provincias p);

-- ¿Cuántas provincias tiene cada comunidad autónoma?
  SELECT p.autonomia, COUNT(*) 
    FROM provincias p 
    GROUP BY p.autonomia;