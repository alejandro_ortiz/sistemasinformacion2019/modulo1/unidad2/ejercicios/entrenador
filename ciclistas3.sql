﻿USE ciclistas;

--  ¿Cuántos ciclistas han ganado etapas llevando un maillot?
  SELECT COUNT(DISTINCT dorsal) 
    FROM lleva 
    JOIN etapa 
    USING (dorsal,numetapa);

-- Cuántos puertos ha ganado Induráin llevando algún maillot
  -- dorsal de indurain
  SELECT dorsal 
    FROM ciclista 
    WHERE nombre='Miguel Induráin';

  -- puertos que ha ganado indurain
  SELECT * FROM puerto 
    JOIN (
      SELECT dorsal 
        FROM ciclista 
        WHERE nombre='Miguel Induráin'
      )c1 
      USING (dorsal);

  -- etapas en las que ha llevado algun maillot
  SELECT DISTINCT numetapa 
    FROM lleva 
    JOIN (
      SELECT dorsal 
        FROM ciclista 
        WHERE nombre='Miguel Induráin'
      )c1 
      USING (dorsal);

  SELECT COUNT(*) 
    FROM (
    SELECT * 
      FROM puerto 
        JOIN (
          SELECT dorsal 
            FROM ciclista 
            WHERE nombre='Miguel Induráin'
          )c1 
          USING (dorsal)
        )c2 
        JOIN (
          SELECT DISTINCT numetapa 
            FROM lleva 
            JOIN (
              SELECT dorsal 
                FROM ciclista 
                WHERE nombre='Miguel Induráin'
              )c1 
              USING (dorsal)
          )c3 USING (numetapa);

-- Etapas con todos sus puertos superando los 1300 metros de altura
  
  SELECT etapa.numetapa 
    FROM etapa 
    JOIN (
      SELECT DISTINCT numetapa 
        FROM puerto 
        WHERE numetapa NOT IN (
          SELECT DISTINCT numetapa 
            FROM puerto 
            WHERE altura<=1300)
        )c1 
        USING (numetapa); 

-- ¿Qué equipos no han ganado etapas con puerto de montaña?

  -- etapas con puerto
  SELECT DISTINCT numetapa, etapa.dorsal  
    FROM etapa 
    JOIN puerto 
    USING(numetapa);

  -- equipos si han ganado etapas con puerto
  SELECT DISTINCT nomequipo 
    FROM ciclista 
    JOIN (SELECT DISTINCT numetapa, etapa.dorsal  
    FROM etapa 
    JOIN puerto 
    USING(numetapa))c1 USING (dorsal); 

  SELECT DISTINCT nomequipo 
    FROM equipo 
    WHERE nomequipo NOT IN (
      SELECT DISTINCT nomequipo 
    FROM ciclista 
    JOIN (SELECT DISTINCT numetapa, etapa.dorsal  
    FROM etapa 
    JOIN puerto 
    USING(numetapa))c1 USING (dorsal)); 

-- ¿Cuántos puertos ha ganado el líder de la montaña?

  SELECT COUNT(*) 
    FROM puerto 
    WHERE dorsal=(
      SELECT dorsal 
        FROM lleva 
        WHERE numetapa=(
          SELECT MAX(numetapa) 
            FROM etapa
          ) 
        AND código='MMO'); 

-- ¿Cuántos kms hay que recorrer para llegar a la etapa con el puerto más alto?

  SELECT MAX(altura) FROM puerto;

  SELECT numetapa 
    FROM puerto 
    WHERE altura=(
      SELECT MAX(altura) 
        FROM puerto) 
        ORDER BY numetapa;

  SELECT SUM(kms) 
    FROM etapa 
    WHERE numetapa BETWEEN 1 AND (
      SELECT numetapa 
        FROM puerto 
        WHERE altura=(
          SELECT MAX(altura) 
            FROM puerto) 
            ORDER BY numetapa 
            LIMIT 1)-1;

  SELECT SUM(kms) FROM etapa WHERE numetapa<(
    SELECT MIN(numetapa) FROM(
      SELECT numetapa FROM puerto WHERE altura=(
        SELECT MAX(altura) FROM puerto
      )
    )c1
  );

-- ¿Cuántos kms hay que recorrer para llegar a la última etapa con el puerto más alto?

  SELECT SUM(kms) FROM etapa WHERE numetapa<(
    SELECT MAX(numetapa) FROM(
      SELECT numetapa FROM puerto WHERE altura=(
        SELECT MAX(altura) FROM puerto
      )
    )c1
  );


 -- Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

  SELECT COUNT(*) FROM 
    (SELECT numetapa,dorsal FROM puerto WHERE dorsal=( 
      SELECT dorsal FROM ciclista 
        WHERE nombre='Miguel Induráin'
      ))c2
      JOIN lleva USING (numetapa, dorsal); 

-- Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado

  SELECT código FROM (
    SELECT DISTINCT numetapa FROM puerto
  )c2 JOIN (
    SELECT * FROM etapa WHERE dorsal=(
      SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin'
    )
  )c2b USING(numetapa)
  JOIN lleva USING(dorsal,numetapa);

-- Obtener las poblaciones de salida y de llegada de las etapas donde se encuentran los puertos con mayor pendiente

  SELECT numetapa 
    FROM puerto 
    WHERE pendiente = (
      SELECT MAX(pendiente) 
        FROM puerto);

  SELECT salida, llegada 
    FROM etapa 
    WHERE numetapa=(
      SELECT numetapa 
        FROM puerto 
        WHERE pendiente = (
          SELECT MAX(pendiente) 
            FROM puerto));

-- Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura

  SELECT dorsal, nombre FROM ciclista JOIN (SELECT dorsal
    FROM puerto 
    WHERE altura=(
      SELECT MAX(altura) 
        FROM puerto) 
        ORDER BY numetapa)c1  USING (dorsal);

