﻿USE ciclistas;

-- ¿Cuál es la distancia total recorrida?
  SELECT SUM(e.kms) 
    FROM etapa e;

-- ¿Cuántos kms se recorren en la vuelta?
  SELECT SUM(e.kms) 
    FROM etapa e;

-- Obtener los datos de las etapas que no comienzan en la misma ciudad en que 
-- acaba la etapa anterior
  SELECT numetapa, salida FROM etapa;

  SELECT numetapa, llegada FROM etapa;

  SELECT c2.numetapa FROM 
    (SELECT numetapa, llegada FROM etapa)c1 
  JOIN 
    (SELECT numetapa, salida FROM etapa)c2
  ON c2.numetapa-1= c1.numetapa
  WHERE llegada!=salida;

  SELECT etapa.numetapa, kms, salida, llegada, dorsal 
    FROM etapa 
   JOIN (
    SELECT c2.numetapa 
      FROM 
        (SELECT numetapa, llegada FROM etapa)c1 
      JOIN 
        (SELECT numetapa, salida FROM etapa)c2
      ON c2.numetapa-1= c1.numetapa
      WHERE llegada!=salida
    )c3 
    USING (numetapa);

-- Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que
-- no se realiza ninguna salida. Resuélvela utilizando NOT IN
  SELECT llegada 
    FROM etapa 
    WHERE llegada NOT IN (
      SELECT salida FROM etapa);

-- ¿Qué ciclistas han ganado etapas llevando un maillot?
  SELECT nombre 
    FROM ciclista 
    JOIN (   
      SELECT DISTINCT etapa.dorsal 
        FROM etapa 
        JOIN lleva
        USING (dorsal,numetapa)
      )c1 
    USING (dorsal);

-- Obtener el nombre de los puertos de montaña que tienen una altura superior a la 
-- altura media de todos los puertos
  -- altura media
  SELECT AVG(p.altura) 
    FROM puerto p;

  SELECT p.nompuerto 
    FROM puerto p 
    WHERE p.altura>(
      SELECT AVG(p.altura) 
        FROM puerto p);

-- Número de la etapa más larga
  -- c1 kms maximo
  SELECT MAX(e.kms) 
    FROM etapa e;

  SELECT e.numetapa 
    FROM etapa e 
    WHERE e.kms=(
      SELECT MAX(e.kms) 
        FROM etapa e);

-- ¿Cuántos maillots diferentes ha llevado Miguel Induráin?

  SELECT c.dorsal 
    FROM ciclista c 
    WHERE c.nombre='Miguel Induráin';

  SELECT COUNT(DISTINCT l.código) 
    FROM lleva l 
    WHERE l.dorsal=(
      SELECT c.dorsal 
        FROM ciclista c 
        WHERE c.nombre='Miguel Induráin');

-- ¿Cuánto dinero se ha repartido en premios?

  -- maillots de cada
  SELECT código, COUNT(código)maillots 
    FROM lleva 
    GROUP BY código;

  SELECT c1.código, c1.maillots, premio FROM maillot JOIN (SELECT código, COUNT(código)maillots 
    FROM lleva 
    GROUP BY código)c1 USING (código);

  SELECT SUM(c2.maillots*premio) 
    FROM (
      SELECT c1.código, c1.maillots, premio 
        FROM maillot 
        JOIN (
          SELECT código, COUNT(código)maillots 
            FROM lleva 
            GROUP BY código)c1 
        USING (código))c2;


-- Obtener los datos de las etapas que pasan por algún puerto de montaña y que tienen
-- salida y llegada en la misma población
  -- c1 - etapas con puerto
  SELECT DISTINCT numetapa 
    FROM puerto; 

  -- c2 - etapa con salida y llegada misma poblacion
  SELECT numetapa 
    FROM etapa 
    WHERE salida= llegada;

  -- c3 - etapa que cumple las consiciones
  SELECT c1.numetapa 
    FROM (
      SELECT DISTINCT numetapa 
        FROM puerto
      )c1 
    JOIN (
      SELECT numetapa 
        FROM etapa 
        WHERE salida= llegada
      )c2 
    USING (numetapa);

  SELECT * 
    FROM etapa 
    WHERE numetapa=(
      SELECT c1.numetapa 
        FROM (
          SELECT DISTINCT numetapa 
            FROM puerto
          )c1 
        JOIN (
          SELECT numetapa 
            FROM etapa 
            WHERE salida= llegada
          )c2 
        USING (numetapa));