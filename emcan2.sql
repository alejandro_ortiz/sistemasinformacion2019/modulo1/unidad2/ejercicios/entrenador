﻿USE emcan;

-- En qué municipio hay más centros de formación

  SELECT municipio 
    FROM centros 
    GROUP BY 1 
    HAVING COUNT(*)=(
      SELECT MAX(n) 
        FROM (
          SELECT municipio, COUNT(*)n 
            FROM centros 
            GROUP BY 1
          )c1);

-- ¿Qué centro impartirá la mayor cantidad de cursos?

  SELECT cod_centro, COUNT(*)n FROM programacion GROUP BY cod_centro ORDER BY n DESC limit 1;

  SELECT cod_centro, MAX(n) 
    FROM (
      SELECT cod_centro, COUNT(*)n 
        FROM programacion 
        GROUP BY cod_centro
        ORDER BY n DESC limit 1 
      )c1;

  SELECT centro FROM ( SELECT cod_centro, MAX(n) 
    FROM (
      SELECT cod_centro, COUNT(*)n 
        FROM programacion 
        GROUP BY cod_centro
    ORDER BY n DESC limit 1
      )c1)c2 JOIN centros USING (cod_centro);

  -- otro forma
    SELECT centro FROM (
      SELECT cod_centro FROM programacion GROUP BY 1
        HAVING COUNT(*)=(
          SELECT MAX(n) FROM (
              SELECT cod_centro,COUNT(*) n
                FROM programacion GROUP BY 1  
            ) c1  
        )  
    ) c2 JOIN centros USING(cod_centro);

-- ¿De qué familia profesional han subvencionado más cursos?

  SELECT SUBSTR(codigo,1,3)fam, COUNT(*)n
    FROM programacion 
    GROUP BY fam 
    ORDER BY n DESC 
    LIMIT 1;

  SELECT fam 
    FROM (
      SELECT SUBSTR(codigo,1,3)fam, COUNT(*)n 
        FROM programacion 
        GROUP BY fam 
        ORDER BY n DESC 
        LIMIT 1
      )c1;

  SELECT familia 
    FROM familias 
    WHERE fml = (
      SELECT fam 
        FROM (
          SELECT SUBSTR(codigo,1,3)fam, COUNT(*)n 
            FROM programacion 
            GROUP BY fam 
            ORDER BY n DESC 
            LIMIT 1
          )c1);

    -- otra forma 
  SELECT familia FROM (
    SELECT SUBSTR(codigo,1,3) fml
      FROM programacion
      GROUP BY 1 HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
            SELECT SUBSTR(codigo,1,3),
              COUNT(*) n
              FROM programacion
              GROUP BY 1
          ) c1  
      )  
  ) c2 JOIN familias USING(fml);


-- ¿En qué familia profesional está especializado cada centro?
-- Indíquese centro, municipio y familia profesional; y ordénese por municipio y especialidad

-- ¿En qué familia profesional está especializado cada municipio? 
-- Muestra y ordena por familia y municipio

-- ¿Qué centro impartirá la mayor cantidad de cursos en Santander?

-- ¿Qué centro impartirá la mayor cantidad de cursos de informática en Cantabria?

-- ¿Cuál es el curso de informática que más se impartirá?

-- ¿Qué centro impartirá la mayor cantidad de cursos de informática de nivel 3 en Cantabria?

-- ¿Qué centros impartirán la mayor cantidad de cursos de informática en Santander?