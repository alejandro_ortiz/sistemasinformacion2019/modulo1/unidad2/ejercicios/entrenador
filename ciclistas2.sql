﻿USE ciclistas;

-- Obtener el número de las etapas que tienen algún puerto de montaña, 
-- indicando cuántos tiene cada una de ellas
  SELECT numetapa, COUNT(*) 
    FROM puerto 
    GROUP BY numetapa;

-- Obtener el nombre y el equipo de los ciclistas que han llevado algún
-- maillot o que han ganado algún puerto. Muestra la lista ordenada
  SELECT DISTINCT nombre, nomequipo 
    FROM  ciclista 
    JOIN lleva 
    USING (dorsal) 
  UNION
  SELECT DISTINCT nombre, nomequipo 
    FROM ciclista 
    JOIN puerto 
    USING (dorsal);

  -- Qué códigos de maillots ha llevado Alfonso Gutiérrez en los puertos que haya ganado
  SELECT dorsal 
    FROM ciclista 
    WHERE nombre='Alfonso Gutiérrez';

  SELECT numetapa 
    FROM puerto 
    WHERE dorsal = (
      SELECT dorsal 
        FROM ciclista 
        WHERE nombre='Alfonso Gutiérrez');

  SELECT código 
    FROM lleva 
    JOIN (
      SELECT numetapa, dorsal 
        FROM puerto 
        WHERE dorsal = (
          SELECT dorsal 
            FROM ciclista 
            WHERE nombre='Alfonso Gutiérrez')
          )c1 
          USING (numetapa) 
          WHERE c1.dorsal= lleva.dorsal;

-- ¿Qué equipos no han ganado ningún puerto de montaña?
  SELECT nomequipo 
    FROM puerto 
    JOIN ciclista 
    USING (dorsal);

  SELECT nomequipo 
    FROM equipo 
    WHERE nomequipo NOT IN (
      SELECT nomequipo 
        FROM puerto 
        JOIN ciclista 
        USING (dorsal));

-- ¿Cuál es la altura y pendiente media de cada categoría de puerto de montaña?
  SELECT categoria, AVG(altura), AVG(pendiente) 
    FROM puerto 
    GROUP BY categoria;

-- Nombre de los ciclistas que han ganado puertos de categoría 2
  SELECT DISTINCT nombre 
    FROM ciclista 
    JOIN puerto 
    USING (dorsal) 
    WHERE categoria=2;

-- Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa llevando 
-- el maillot amarillo, mostrando también el número de etapa
  SELECT nombre,nomequipo,numetapa 
    FROM ciclista 
    JOIN etapa 
    USING (dorsal) 
    JOIN lleva 
    USING (numetapa,dorsal) 
    WHERE código='MGE';

-- ¿Qué porcentaje de los kms de la vuelta se recorren en etapas circulares?  
  SELECT (
    SELECT SUM(kms) 
      FROM etapa 
      WHERE salida=llegada)/(
    SELECT SUM(kms) 
      FROM etapa)*100;

-- Obtener la edad media de los ciclistas que han ganado alguna etapa
  SELECT AVG(edad) 
    FROM ciclista 
    JOIN (
      SELECT DISTINCT dorsal 
        FROM etapa
      )c1 
    USING (dorsal);

-- Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura

  SELECT DISTINCT numetapa FROM puerto WHERE altura<=1300;
   
  SELECT etapa.* 
    FROM etapa 
    JOIN (
      SELECT DISTINCT numetapa 
        FROM puerto 
        WHERE numetapa NOT IN (
          SELECT DISTINCT numetapa 
            FROM puerto 
            WHERE altura<=1300)
        )c1 
        USING (numetapa); 